import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:google_fonts/google_fonts.dart';

class ImportarTablaPage extends StatefulWidget {
  ImportarTablaPage({Key key}) : super(key: key);

  @override
  _ImportarTablaPageState createState() => _ImportarTablaPageState();
}

class _ImportarTablaPageState extends State<ImportarTablaPage> {

  TextEditingController controlador = new TextEditingController();

  bool cambio = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Importar'),
        backgroundColor: Colors.pink[500],
      ),
      body: Container(
          color: Colors.indigo[50],
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/tabla.png',
                    width: MediaQuery.of(context).size.height * 0.3,
                  ),
                ],
              ),
              tablaMostrar(),
              AnimatedCrossFade(
                  firstCurve: Curves.easeInOutCirc,
                  firstChild: exportarBoton(),
                  secondChild: textoExportado(),
                  crossFadeState: cambio
                      ? CrossFadeState.showFirst
                      : CrossFadeState.showSecond,
                  duration: const Duration(seconds: 2)),
            ],
          )),
    );
  }

  Widget exportarBoton() {
    return ClipPath(
      clipper: OvalTopBorderClipper(),
      child: Container(
        padding: EdgeInsets.only(top: 70, bottom: 50, left: 10, right: 10),
        decoration: BoxDecoration(
          color: Colors.indigo[100]
        ),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10)
                ),
                padding: EdgeInsets.all(15),
                margin: EdgeInsets.only(bottom: 10),
                child: TextField(
                controller: controlador,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    icon: Icon(Icons.bookmark_border),
                    labelText: 'Nombre del documento',
                    
                    helperText: 'Es el nombre como se guardara el documento',
                  //counterText: 
                    suffix: Text('.xls')
                  ),
                ),
              ),

              FlatButton(
                splashColor: Colors.indigo[500].withOpacity(0.6),
                onPressed: () async {
                  setState(() {
                    cambio = false;

                    
                  });
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                padding: EdgeInsets.all(0.0),
                child: Ink(
                  padding: EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Colors.pink[500].withOpacity(0.7),
                          Colors.indigo.withOpacity(0.7)
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.save_alt,
                        color: Colors.white,
                      ),
                      Container(
                        padding: EdgeInsets.all(0),
                        alignment: Alignment.center,
                        child: Text(
                          " Exportar .xls",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.adventPro(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget textoExportado() {
    return Container(
      padding: EdgeInsets.only(top:10.0, bottom: 20.0),
      child: Center(
        child: Column(
          children: <Widget>[
            Text('Guardado con exito en tu telefono con el nombre'),
            Text('${controlador.text}.xls', style: TextStyle(
              fontWeight: FontWeight.w800
            ),)
          ],
        ),
      ),
    );
  }

  Widget tablaMostrar() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        margin: EdgeInsets.all(10.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.white12,
        ),
        child: DataTable(
          //sortAscending: true,
          columns: const <DataColumn>[
            DataColumn(
              label: Text(
                '0',
              ),
            ),
            DataColumn(
              label: Text(
                '1',
              ),
            ),
            DataColumn(
              label: Text(
                '2',
              ),
            ),
            DataColumn(
              label: Text(
                '3',
              ),
            ),
            DataColumn(
              label: Text(
                '4',
              ),
            ),
            DataColumn(
              label: Text(
                '5',
              ),
            ),
            DataColumn(
              label: Text(
                '6',
              ),
            ),
            DataColumn(
              label: Text(
                '7',
              ),
            ),
            DataColumn(
              label: Text(
                '8',
              ),
            ),
            DataColumn(
              label: Text(
                '9',
              ),
            ),
          ],
          rows: const <DataRow>[
            DataRow(
              cells: <DataCell>[
                DataCell(Text('1')),
                DataCell(Text('2')),
                DataCell(Text('3')),
                DataCell(Text('4')),
                DataCell(Text('5')),
                DataCell(Text('6')),
                DataCell(Text('7')),
                DataCell(Text('8')),
                DataCell(Text('9')),
                DataCell(Text('10')),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text('2')),
                DataCell(Text('3')),
                DataCell(Text('4')),
                DataCell(Text('5')),
                DataCell(Text('6')),
                DataCell(Text('7')),
                DataCell(Text('8')),
                DataCell(Text('9')),
                DataCell(Text('10')),
                DataCell(Text('11')),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text('3')),
                DataCell(Text('4')),
                DataCell(Text('5')),
                DataCell(Text('6')),
                DataCell(Text('7')),
                DataCell(Text('8')),
                DataCell(Text('9')),
                DataCell(Text('10')),
                DataCell(Text('11')),
                DataCell(Text('12')),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text('4')),
                DataCell(Text('5')),
                DataCell(Text('6')),
                DataCell(Text('7')),
                DataCell(Text('8')),
                DataCell(Text('9')),
                DataCell(Text('10')),
                DataCell(Text('11')),
                DataCell(Text('12')),
                DataCell(Text('13')),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text('5')),
                DataCell(Text('6')),
                DataCell(Text('7')),
                DataCell(Text('8')),
                DataCell(Text('9')),
                DataCell(Text('10')),
                DataCell(Text('11')),
                DataCell(Text('12')),
                DataCell(Text('13')),
                DataCell(Text('14')),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text('6')),
                DataCell(Text('7')),
                DataCell(Text('8')),
                DataCell(Text('9')),
                DataCell(Text('10')),
                DataCell(Text('11')),
                DataCell(Text('12')),
                DataCell(Text('13')),
                DataCell(Text('14')),
                DataCell(Text('15')),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text('7')),
                DataCell(Text('8')),
                DataCell(Text('9')),
                DataCell(Text('10')),
                DataCell(Text('11')),
                DataCell(Text('12')),
                DataCell(Text('13')),
                DataCell(Text('14')),
                DataCell(Text('15')),
                DataCell(Text('16')),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text('8')),
                DataCell(Text('9')),
                DataCell(Text('10')),
                DataCell(Text('11')),
                DataCell(Text('12')),
                DataCell(Text('13')),
                DataCell(Text('14')),
                DataCell(Text('15')),
                DataCell(Text('16')),
                DataCell(Text('17')),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text('9')),
                DataCell(Text('10')),
                DataCell(Text('11')),
                DataCell(Text('12')),
                DataCell(Text('13')),
                DataCell(Text('14')),
                DataCell(Text('15')),
                DataCell(Text('16')),
                DataCell(Text('17')),
                DataCell(Text('18')),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
