import 'dart:io';

import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:scanxel/pages/importar_tabla_page.dart';

/* class Tabla {
  final int col;
  final String valor;
  Tabla({this.col, this.valor});
} */

class MostrarCapturaPage extends StatefulWidget {
  final String imagePath;
  final CameraDescription camera;
  MostrarCapturaPage({Key key, this.imagePath, this.camera}) : super(key: key);

  @override
  _MostrarCapturaPageState createState() => _MostrarCapturaPageState();
}

class _MostrarCapturaPageState extends State<MostrarCapturaPage> {
  String text = '';
  List<List<String>> elementos = new List();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future scanImage() async {
    FirebaseVisionImage visionImage =
        FirebaseVisionImage.fromFilePath(widget.imagePath);
    TextRecognizer textRecognizer = FirebaseVision.instance.textRecognizer();
    VisionText visionText = await textRecognizer.processImage(visionImage);

    int i = 1;

    for (TextBlock block in visionText.blocks) {
      for (TextLine line in block.lines) {
        //print('linea $i');

        for (TextElement word in line.elements) {
          setState(() {
            text = word.text + ' \n';
          });
          elementos.add([i.toString(), text]);
        }
      }

      //elementos.add([block.text]);
      i++;
    }

    print(elementos);

    //print("Bloques: $text ");
    //print(text);
    textRecognizer.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Procesar'),
        backgroundColor: Colors.indigo[600],
      ),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Container(
          color: Colors.indigo[50],
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              parteTop(context),
              Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white,
                  ),
                  margin: EdgeInsets.all(5),
                  padding: EdgeInsets.all(5),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(3),
                      child: Image.file(File(widget.imagePath)))),
              parteBottom(context),
            ],
          )),
    );
  }

  ClipPath parteBottom(BuildContext context) {
    return ClipPath(
      clipper: WaveClipperTwo(flip: false, reverse: true),
      child: Container(
        padding: EdgeInsets.only(top: 70, bottom: 50, left: 10, right: 10),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/fondo_tec.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          child: FlatButton(
            splashColor: Colors.indigo[700].withOpacity(0.6),
            onPressed: () async {
              await scanImage();

              Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ImportarTablaPage()
                    ),
                  );
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            padding: EdgeInsets.all(0.0),
            child: Ink(
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.teal[500].withOpacity(0.7),
                      Colors.yellow.withOpacity(0.7)
                    ],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                  borderRadius: BorderRadius.circular(10.0)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.settings_overscan,
                    color: Colors.white,
                  ),
                  Container(
                    padding: EdgeInsets.all(0),
                    alignment: Alignment.center,
                    child: Text(
                      " Escanear Imagen",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.adventPro(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

//
  ClipPath parteTop(BuildContext context) {
    return ClipPath(
      clipper: WaveClipperTwo(),
      child: Container(
        padding: EdgeInsets.only(top: 10, bottom: 20, left: 10, right: 10),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/fondo_chalco.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage(
                'assets/excel.png',
              ),
              height: MediaQuery.of(context).size.height * 0.3,
              width: MediaQuery.of(context).size.width * 0.4,
            ),
            Expanded(
              child: Text(
                'Que empiece el escaneo!',
                style: GoogleFonts.adventPro(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.normal),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
