import 'package:flutter/material.dart';

import 'package:camera/camera.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:scanxel/pages/captura_page.dart';

class PrincipalPage extends StatefulWidget {
  final CameraDescription camera;

  PrincipalPage({Key key, this.camera}) : super(key: key);

  @override
  _PrincipalPageState createState() => _PrincipalPageState();
}

class _PrincipalPageState extends State<PrincipalPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        centerTitle: true,
        title: Center(child: Text('Scanxel')),
        backgroundColor: Colors.teal[400],
        elevation: 0.0,
      ),
      body: contenidoPrincipal(),
      floatingActionButton: getFloatingActionButton(widget.camera),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,

      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  FloatingActionButton getFloatingActionButton(CameraDescription camera) {
    return FloatingActionButton(
      onPressed: () {
        //Navigator.pushNamed(context, 'captura', arguments: );
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => CapturaPage(camera: camera)),
        );
      },
      tooltip: 'Escanear',
      splashColor: Color.fromRGBO(200, 150, 50, 1),
      child: Icon(Icons.photo_camera),
      backgroundColor: Color.fromRGBO(200, 50, 50, 1),
      elevation: 1.0,
    );
  }

  Widget contenidoPrincipal() {
    return ListView(shrinkWrap: true, children: <Widget>[
      contenidoTop(),
      cardsCore(
          title: 'Capturar',
          description:
              'Toma una fotografia en la cual contenga informacion que se pueda trasladar a una tabla de Excel',
          icon: Icon(Icons.camera_alt, color: Colors.white),
          image: 'assets/card-1.jpg'),
      cardsCore(
          title: 'Escanear',
          description:
              'Se identificara el texto en la imagen y posteriormente el resultado',
          icon: Icon(Icons.scanner, color: Colors.white),
          image: 'assets/card-2.jpg'),
      cardsCore(
          title: 'Exportar / Compartir',
          description:
              'Almacena en tu dispositivo el resultado en un archivo .xls para Excel',
          icon: Icon(Icons.save_alt, color: Colors.white),
          image: 'assets/card-3.jpg'),
    ]);
  }

  Container cardsCore(
      {String title, String description, Icon icon, String image}) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(image),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(4)),
      margin: EdgeInsets.all(6),
      padding: EdgeInsets.all(6),
      child: ListTile(
        title: Text(title,
            style: GoogleFonts.adventPro(
                fontSize: 19,
                color: Colors.white54,
                fontWeight: FontWeight.bold)),
        subtitle: Text(description,
            style: GoogleFonts.adventPro(fontSize: 17, color: Colors.white)),
        leading: icon,
      ),
    );
  }

  Widget contenidoTop() {
    return ClipPath(
      clipper: MultipleRoundedCurveClipper(),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/fondo_principal.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(24.0),
        alignment: Alignment.topCenter,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image(
              image: AssetImage(
                'assets/scanxel-logo.png',
              ),
              width: MediaQuery.of(context).size.width * 0.5,
              //height: MediaQuery.of(context).size.height * 0.5
              //fit: BoxFit.cover,
            ),
            Divider(),
            Text(
              '"La aplicación que te hace la vida mas facil" \n\nToma fotografias de algun libro con tablas y conviertelas en archivo de Excel de la manera mas facil de la que te puedas imaginar!',
              style: GoogleFonts.adventPro(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
