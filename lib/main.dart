import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:scanxel/pages/principal_page.dart';
import 'package:scanxel/routes/routes.dart';

Future<void> main() async {
  // Ensure that plugin services are initialized so that `availableCameras()`
  // can be called before `runApp()`
  WidgetsFlutterBinding.ensureInitialized();

  // Obtain a list of the available cameras on the device.
  final cameras = await availableCameras();

  // Get a specific camera from the list of available cameras.
  final firstCamera = cameras.first;
  print('datos: $firstCamera');
  runApp(MyApp(camera: firstCamera));
}
// A screen that allows users to take a picture using a given camera.

class MyApp extends StatelessWidget {
  final CameraDescription camera;
  MyApp({this.camera});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: PrincipalPage(camera: camera),
      //initialRoute: 'captura',
      routes: getApplicationRoutes(),
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          elevation: 0.0,
          textTheme: TextTheme(
              headline6: GoogleFonts.monofett(
            fontSize: 28,
            fontWeight: FontWeight.normal,
          )),
        ),
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}
